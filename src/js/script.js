"use strict";
let menuButton = document.querySelector(".header__button");
let headerList = document.querySelector(".header__list");
menuButton.addEventListener("click", () => {
  headerList.classList.toggle("header__list--opened");
});
